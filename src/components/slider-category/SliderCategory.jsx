// import Swiper core and required modules
import { Pagination } from "swiper";
import React, { useState,useEffect,useRouter } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";

import "./sliderCategory.css";

const SliderCategory = () => {
  const [slideItems] = useState([
    {
      title: "Sprite",
      icon: "./img/category-slider/category-slider-2.svg",
      id: 1,
    },
    {
      title: "Coca Cola",
      icon: "./img/category-slider/category-slider-1.svg",
      id: 2,
    },
    {
      title: "Sprite",
      icon: "./img/category-slider/category-slider-2.svg",
      id: 3,
    },
    {
      title: "Coca Cola",
      icon: "./img/category-slider/category-slider-1.svg",
      id: 4,
    },
    {
      title: "Sprite",
      icon: "./img/category-slider/category-slider-2.svg",
      id: 5,
    },
    {
      title: "Coca Cola",
      icon: "./img/category-slider/category-slider-1.svg",
      id: 6,
    },
  ]);
  const [filterButtons] = useState([
    {
      title: "Fırsat Bul",
      icon: "./img/category-slider/search-icon.svg",
      id: 1,
    },
    {
      title: "Coca Cola",
      icon: "./img/category-slider/coca-cola-icon.svg",
      id: 2,
    },
    { title: "Sprite", icon: "./img/category-slider/sprite-icon.svg", id: 3 },
    {
      title: "Trendyol",
      icon: "./img/category-slider/trendyol-icon.svg",
      id: 4,
    },
  ]);
  const [slideItemsCopy, setSlideItemsCopy] = useState(slideItems);

  const filterSliders = (buttonTitle) => {
    let deepCopy = JSON.parse(JSON.stringify(slideItems));
    let filteredItems = deepCopy.filter((item) => item.title === buttonTitle);
    setSlideItemsCopy(filteredItems);
  };

 /* const [posts, setPosts] = useState([]);
  const router = useRouter();

  useEffect(() => {
    let myHeaders = new Headers();
    myHeaders.append("test", "123");

    let raw = "";

    let requestOptions = {
      method: "GET",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch("https://loremipsum.com/", requestOptions)
      .then((response) => response.json())
      .then((result) => setSlideItemsCopy(result))
      .catch((error) => console.log("error", error));
  }, []); v
  */
  const [users, setUsers] = useState([]);

  const getUsers = async () => {
    const response = await fetch("https://jsonplaceholder.typicode.com/users");
    let users = await response.json();
    setUsers(users);
  };

  useEffect(() => {
    getUsers();
  }, []);

  console.log(users);

  return (
    <>
      <div className="category-slider">
        <div className="container">
          <div className="slider-category-tabs">
            <ul>
              {filterButtons.map((item) => (
                <li
                  className="slider-item"
                  onClick={() => filterSliders(item.title)}
                >
                  <img
                    className="tab-icons"
                    src={item.icon}
                    alt={item.title}
                    key={item.id}
                  />
                  <span>{item.title}</span>
                </li>
              ))}
            </ul>
          </div>
          <Swiper
            spaceBetween={15}
            slidesPerView={4}
            modules={[Pagination]}
            pagination={{ clickable: true }}
            breakpoints={{
              1300: {
                slidesPerView: 4,
                spaceBetween: 15,
              },
              1100: {
                slidesPerView: 3.5,
                spaceBetween: 50,
              },
              950: {
                slidesPerView: 3,
                spaceBetween: 0,
              },
              800: {
                slidesPerView: 2.5,
                spaceBetween: -10,
              },
              600: {
                slidesPerView: 2,
                spaceBetween: 20,
              },
              500: {
                slidesPerView: 1.5,
                spaceBetween: 10,
              },
              400: {
                slidesPerView: 1.25,
                spaceBetween: 10,
              },
              100: {
                slidesPerView: 1,
                spaceBetween: 10,
              },
            }}
          >
            {slideItemsCopy.map((item) => (
              <SwiperSlide key={item.id}>
                <a
                  href={`/${item.title
                    .toLowerCase()
                    .replace(/[^A-Z0-9]/gi, "-")}`}
                >
                  <img src={item.icon} alt="Slider image" />
                </a>
              </SwiperSlide>
            ))}
          </Swiper>
        </div>
      </div>
    </>
  );
};

export default SliderCategory;
